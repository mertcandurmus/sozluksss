import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class BlogService {

  options;
  domain = this.authService.domain;

  constructor(
    private authService: AuthService,
    private http: HttpClient
  ) { }

  newBlog(blog) {
    this.authService.loadToken();
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.authService.authToken });
    return this.http.post(this.domain + 'blogs/newBlog', blog, { headers });
  }

  getAllBlogs() {
    this.authService.loadToken();
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.authService.authToken });
    return this.http.get(this.domain + 'blogs/allBlogs', { headers });
}


getSingleBlog(id) {
  this.authService.loadToken();
  const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.authService.authToken });
  return this.http.get(this.domain + 'blogs/singleBlog/' + id, { headers });
}


editBlog(blog) {
  this.authService.loadToken();
  const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.authService.authToken });
  return this.http.put(this.domain + 'blogs/updateBlog/', blog, { headers });
}


deleteBlog(id) {
  this.authService.loadToken();
  const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.authService.authToken });
  return this.http.delete(this.domain + 'blogs/deleteBlog/' + id, { headers });
}


likeBlog(id) {
  const blogData = { id };
  const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.authService.authToken });
  return this.http.put(this.domain + 'blogs/likeBlog/', blogData, { headers });
}


dislikeBlog(id) {
  const blogData = { id };
  const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.authService.authToken });
  return this.http.put(this.domain + 'blogs/dislikeBlog/', blogData, { headers });
}

postComment(id, comment) {
  this.authService.loadToken();
  const blogData = {
    id,
    comment
  };
  const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.authService.authToken });
  return this.http.post(this.domain + 'blogs/comment', blogData, { headers });

}

}
