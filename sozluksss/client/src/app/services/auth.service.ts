import { Injectable } from '@angular/core';
import { tokenNotExpired } from 'angular2-jwt';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class AuthService {

  domain = 'http://localhost:8080/';
  authToken;
  user;
  options;

  constructor(
    private http: HttpClient
  ) { }

  loadToken() {
    this.authToken = localStorage.getItem('token');
  }

  registerUser(user) {
    return this.http.post(this.domain + 'authentication/register', user);
  }

  checkUsername(username) {
    return this.http.get(this.domain + 'authentication/checkUsername/' + username);
  }

  checkEmail(email) {
    return this.http.get(this.domain + 'authentication/checkEmail/' + email);
  }

  login(user) {
    return this.http.post(this.domain + 'authentication/login', user);
  }

  logout() {
    this.authToken = null;
    this.user = null;
    localStorage.clear();
  }

  storeUserData(token, user) {
    localStorage.setItem('token', token);
    localStorage.setItem('user', JSON.stringify(user));
    this.authToken = token;
    this.user = user;
  }


  getProfile() {
    this.loadToken();
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.authToken });
    return this.http.get(this.domain + 'authentication/profile', { headers });
  }

  getPublicProfile(username) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json', Authorization: this.authToken });
    return this.http.get(this.domain + 'authentication/publicProfile/' + username, { headers });
}

  loggedIn() {
    return tokenNotExpired();
  }



}
