import { Component } from '@angular/core';
import { FlashMessagesService, FlashMessagesModule } from 'angular2-flash-messages';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Hello World from Angular';
}
