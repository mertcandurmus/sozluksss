import { Component, OnInit } from '@angular/core';
import { BlogService } from '../../../services/blog.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-delete-blog',
  templateUrl: './delete-blog.component.html',
  styleUrls: ['./delete-blog.component.css']
})
export class DeleteBlogComponent implements OnInit {
  message;
  messageClass;
  foundBlog = false;
  processing = false;
  blog;
  currentUrl;

  constructor(
    private blogService: BlogService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }


  deleteBlog() {
    this.processing = true;
    this.blogService.deleteBlog(this.currentUrl.id).subscribe(data => {
      if (!(data as any).success) {
        this.messageClass = 'alert alert-danger';
        this.message = (data as any).message;
      } else {
        this.messageClass = 'alert alert-success';
        this.message = (data as any).message;
        setTimeout(() => {
          this.router.navigate(['/blog']);
        }, 2000);
      }
    });
  }

  ngOnInit() {
    this.currentUrl = this.activatedRoute.snapshot.params;
    this.blogService.getSingleBlog(this.currentUrl.id).subscribe(data => {
      if (!(data as any).success) {
        this.messageClass = 'alert alert-danger';
        this.message = (data as any).message;
      } else {
        this.blog = {
          title: (data as any).blog.title,
          body: (data as any).blog.body,
          createdBy: (data as any).blog.createdBy,
          createdAt: (data as any).blog.createdAt
        };
        this.foundBlog = true;
      }
    });
  }

}
